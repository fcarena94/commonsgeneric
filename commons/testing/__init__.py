from abc import abstractmethod, ABC
from os import environ
from unittest import TestCase

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from nwcommons.app.flask_app import db
import pytest
from alembic.command import downgrade, upgrade
from alembic.config import Config

from nwcommons.db import init_engine, get_session


class BaseTest(TestCase):
    """Base class for tests."""

    def __init__(self, *args, **kwargs):
        TestCase.__init__(self, *args, **kwargs)
        self.session = None

    @pytest.fixture(autouse=True)
    def mocker_fix(self, mocker):
        """Set a mocker instance.

        Parameters
        ----------
        mocker : pytest mocker
            mocker instance.

        """

        self.mocker = mocker

    def set_test_client(self, application):
        """Creates a test client and request headers.

        Parameters
        ----------
        application : Flask app
            flask application for clients creation.

        """

        application.testing = True
        application.app_context().push()
        self.client = application.test_client()
        self.headers = {
            "content-type": "application/json",
            "cache-control": "no-cache"
        }

    def run_migrations(self):
        """Runs alembic migrations and creates database session.

        Checks if both env vars ALEMBIC_INI_PATH and DB_CONN_STRING are set.
        If so then check if the session instance is set and create it if not.
        After the session is set, run the alembic migrations.
        """
        if any([
            environ.get('ALEMBIC_INI_PATH') is None,
            environ.get('DB_CONN_STRING') is None
        ]):
            raise Exception(
                "Env vars missing: DB_CONN_STRING or ALEMBIC_INI_PATH"
            )

        if self.session is None:
            self._create_db_setup()

        self._reset_db()
        TestCase.setUp(self)

    def _reset_db(self):
        """Reset database."""
        self.tearDown()
        if self.session is not None:
            upgrade(self.alembic_cfg, 'head')

    def tearDown(self):
        """Tear down database."""
        if self.session is not None:
            self.session.rollback()
            downgrade(self.alembic_cfg, 'base')
            db.Model.metadata.drop_all(db.engine)
        TestCase.tearDown(self)

    def _create_db_setup(self):
        """Creates the DB session with the corresponding alembic config."""
        app = Flask(__name__)
        app.config['SQLALCHEMY_DATABASE_URI'] = environ.get('DB_CONN_STRING')
        app.config['ALEMBIC_INI_PATH'] = environ.get('ALEMBIC_INI_PATH')
        self.set_test_client(app)
        db = SQLAlchemy(app=app)

        self.session = init_engine(db.session)
        self.alembic_cfg = Config(environ.get("ALEMBIC_INI_PATH"))


class FlaskAppTestCase(TestCase, ABC):
    """Base class for testing flask application."""

    @property
    @abstractmethod
    def application(self):
        """Flask application instance."""
        pass

    @property
    @abstractmethod
    def config_class(self):
        """Class with configuration variables.
        ALEMBIC_INI_PATH, SQLALCHEMY_DATABASE_URI
        """
        pass

    def setUp(self):
        """Initialize test variables and flask app."""
        self.app_context = self.application.app_context()
        self.app_context.push()
        self.set_test_client()

    def tearDown(self):
        """Rollback session and clean db for next test."""
        self.app_context.pop()

    @pytest.fixture(autouse=True)
    def mocker_fix(self, mocker):
        """Set a mocker instance.

        Parameters
        ----------
        mocker : py.test mocker
            mocker instance.
        """
        self.mocker = mocker

    def set_test_client(self):
        """Creates a test client and request headers."""
        self.application.testing = True
        self.client = self.application.test_client()
        self.headers = {
            "content-type": "application/json",
            "cache-control": "no-cache"
        }


class FlaskAppAlchemyTestCase(FlaskAppTestCase, ABC):
    """Base class for testing flask application."""

    def setUp(self):
        """Initialize test variables and flask app."""
        super().setUp()
        self.alembic_cfg = Config(self.config_class.ALEMBIC_INI_PATH)
        self.session = init_engine(get_session())
        upgrade(self.alembic_cfg, 'head')

    def tearDown(self):
        """Rollback session and clean db for next test."""
        from nwcommons.app.flask_app import db
        if self.session is not None:
            self.session.remove()
        downgrade(self.alembic_cfg, 'base')
        db.Model.metadata.drop_all(db.engine)
        super().tearDown()
