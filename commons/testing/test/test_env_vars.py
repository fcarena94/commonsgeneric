from os import environ
from unittest import TestCase

from nwcommons.testing import BaseTest
from nwcommons.testing.test import init_file_path


class TestEnvVars(TestCase):

    def setUp(self):
        environ['ALEMBIC_INI_PATH'] = init_file_path + '/alembic.ini'
        environ['DB_CONN_STRING'] = 'sqlite:///' + init_file_path + 'test.db'

    def test_base_test_instantiation_without_env_vars(self):
        environ.clear()
        base_test_class = BaseTest()

        assert isinstance(base_test_class, BaseTest)

    def test_alembic_ini_path_not_set_raises_error(self):
        environ.pop("ALEMBIC_INI_PATH")
        base_test_class = BaseTest()

        with self.assertRaises(Exception) as error:
            base_test_class.run_migrations()

        expected_ex = Exception(
                "Env vars missing: DB_CONN_STRING or ALEMBIC_INI_PATH"
            )

        assert (str(error.exception) == str(expected_ex))

    def test_db_conn_string_not_set_raises_error(self):
        environ.pop("DB_CONN_STRING")
        base_test_class = BaseTest()

        with self.assertRaises(Exception) as error:
            base_test_class.run_migrations()

        expected_ex = Exception(
                "Env vars missing: DB_CONN_STRING or ALEMBIC_INI_PATH"
            )

        assert (str(error.exception) == str(expected_ex))

    def test_alembic_ini_path_set_and_db_conn_string_set_not_raises_error(self):
        base_test_class = BaseTest()
        base_test_class.run_migrations()
