import io
import json
import logging
from unittest import TestCase

from nwcommons.logger import get_logger, CustomJsonFormatter, log_format


class TestLoggerFormatter(TestCase):

    def test_logger_formatter_obfuscation(self):
        logger = get_logger('test')
        log_capture_string = io.StringIO()
        ch = logging.StreamHandler(log_capture_string)
        ch.setLevel(logging.DEBUG)
        formatter = CustomJsonFormatter(log_format)
        ch.setFormatter(formatter)
        logger.addHandler(ch)

        logger.error({
            'password': 'secret_pass',
            'other': 'no hash',
            'pin': 'secret_pin',
            'number': '1234567890123456',
            'card_number': '1234567890123456',
            'nested': {
                'password': 'nested_secret'
            }
        })
        log_contents = json.loads(log_capture_string.getvalue())
        log_capture_string.close()

        assert log_contents.get('number') == '1234########3456'
        assert log_contents.get('card_number') == '1234########3456'
        assert log_contents.get('password') == '###########'
        assert log_contents.get('pin') == '##########'
        assert log_contents.get('nested').get('password') == '#############'

    def test_logger_formatter_big_params(self):
        logger = get_logger('test')
        log_capture_string = io.StringIO()
        ch = logging.StreamHandler(log_capture_string)
        ch.setLevel(logging.DEBUG)
        formatter = CustomJsonFormatter(log_format)
        ch.setFormatter(formatter)
        logger.addHandler(ch)

        logger.info({
            'ocr': 'ocr',
            'selfie': 'selfie',
            'face_id': 'face_id',
            'base64_front': 'base64_front',
            'base64_back': 'base64_back',
            'base64_selfie': 'base64_selfie',
            'file_content': 'file_content',
            'phone_contacts': ['phone_contacts'],
            'phone_numbers': ['phone_numbers'],
            'selfieList': ['selfie1', 'selfie2']
        })
        log_contents = json.loads(log_capture_string.getvalue())
        log_capture_string.close()

        assert log_contents.get('ocr') == '--big-content--'
        assert log_contents.get('selfie') == '--big-content--'
        assert log_contents.get('face_id') == '--big-content--'
        assert log_contents.get('base64_front') == '--big-content--'
        assert log_contents.get('base64_back') == '--big-content--'
        assert log_contents.get('base64_selfie') == '--big-content--'
        assert log_contents.get('file_content') == '--big-content--'
        assert log_contents.get('phone_contacts') == '--big-content--'
        assert log_contents.get('phone_numbers') == '--big-content--'
        assert log_contents.get('selfieList') == '--big-content--'

    def test_logger_formatter_request_id(self):
        logger = get_logger('test')
        log_capture_string = io.StringIO()
        ch = logging.StreamHandler(log_capture_string)
        ch.setLevel(logging.DEBUG)
        formatter = CustomJsonFormatter(log_format)
        ch.setFormatter(formatter)
        logger.addHandler(ch)

        logger.info(
            {
                'headers':
                    {
                        'X-Request-ID': 'HOLA-SOY-UN-REQUEST-ID'
                    }
            }
        )
        log_contents = json.loads(log_capture_string.getvalue())
        log_capture_string.close()

        assert log_contents.get('request_id') == 'HOLA-SOY-UN-REQUEST-ID'