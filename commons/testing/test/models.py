from sqlalchemy import Column, String

from nwcommons.db import Base


class BaseNameModel(Base):
    """Abstraction with name fields.

    Attributes
    ----------
    name : str
        string name.
    """
    __tablename__ = "test"
    name = Column(String, nullable=False)
