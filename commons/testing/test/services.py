class ExampleService:

    @staticmethod
    def get_data():
        return "data"

    @staticmethod
    def sum_numbers(n1, n2):
        return n1 + n2
