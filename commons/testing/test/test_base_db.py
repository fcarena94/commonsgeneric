from os import environ

from nwcommons.testing import BaseTest
from nwcommons.testing.test import init_file_path
from nwcommons.testing.test.models import BaseNameModel


class TestBase(BaseTest):
    """Class that wraps all BaseTest class related tests."""

    def setUp(self):
        environ['ALEMBIC_INI_PATH'] = init_file_path + '/alembic.ini'
        environ['DB_CONN_STRING'] = 'sqlite:///' + init_file_path + 'test.db'
        self.run_migrations()

    def test_base_db_run_migrations_with_alembic_file(self):
        test_register = BaseNameModel(name='testname')
        self.session.add(test_register)
        assert test_register.id is None

        self.session.commit()
        assert test_register.id is not None

        model = BaseNameModel.query.first()
        assert model == test_register
