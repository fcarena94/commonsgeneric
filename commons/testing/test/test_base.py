import json
from os import environ
from unittest import mock

from flask import jsonify

from nwcommons.app import bootstrap_app
from nwcommons.testing import BaseTest
from nwcommons.testing.test.services import ExampleService


class TestBaseMocker(BaseTest):
    """Class that wraps all BaseTest class related tests."""

    def setUp(self):
        environ['FLASK_SECRET_KEY'] = 'KEY'
        self.mock_app = bootstrap_app()

        self.set_test_client(self.mock_app)

        def get_data():
            result = ExampleService.get_data()
            return jsonify({'result': result})

        def sum_nums():
            result = ExampleService.sum_numbers(2, 2)
            return jsonify({'result': result})

        self.mock_app.add_url_rule(
            "/getData",
            "get_data",
            get_data
        )

        self.mock_app.add_url_rule(
            "/sumNums",
            "sum_nums",
            sum_nums
        )

    def test_mock_client_is_set(self):
        with self.mock_app.app_context():
            response = self.client.get(
                '/getData'
            )

        assert response.status_code == 200
        assert json.loads(response.data) == {'result': "data"}

    def test_mocker_patch_object(self):
        get_data_mock = self.mocker.patch.object(
            ExampleService,
            'get_data'
        )
        get_data_mock.return_value = "mocked"

        sum_numbers_mock = self.mocker.patch.object(
            ExampleService,
            'sum_numbers'
        )
        sum_numbers_mock.return_value = 5

        with self.mock_app.app_context():
            response = self.client.get(
                '/getData'
            )

        get_data_mock.assert_called_once()
        assert response.status_code == 200
        assert json.loads(response.data) == {'result': "mocked"}

        with self.mock_app.app_context():
            response = self.client.get(
                '/sumNums'
            )
        sum_numbers_mock.assert_called_with(2, 2)
        assert response.status_code == 200
        assert json.loads(response.data) == {'result': 5}

    @mock.patch('nwcommons.testing.test.services.ExampleService.sum_numbers')
    def test_mock_decorator(self, mock_service):
        mock_service.return_value = "mocked"

        with self.mock_app.app_context():
            response = self.client.get(
                '/sumNums'
            )

        mock_service.assert_called_once()
        assert response.status_code == 200
        assert json.loads(response.data) == {'result': "mocked"}
