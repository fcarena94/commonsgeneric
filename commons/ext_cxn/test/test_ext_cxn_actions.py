from http import HTTPStatus
from unittest import TestCase
from unittest.mock import Mock

import pytest

from nwcommons.ext_cxn.actions.common import (
    RaiseErrorAction,
    RaiseExceptionAction,
    ReturnJsonOrEmptyDictAction,
    ReturnJsonOrErrorMessageAction
)
from nwcommons.ext_cxn.actions.exceptions import (
    ExternalApiError,
    ExternalApiException
)


class TestExtCxnActions(TestCase):
    @pytest.fixture(autouse=True)
    def mocker_fix(self, mocker):
        self.mocker = mocker

    def setUp(self):
        super().setUp()

        self.response = Mock()

    def test_raise_error_action(self):
        mock_json = 'mock json str'
        self.response.configure_mock(**{"json.return_value": mock_json})

        with self.assertRaisesRegex(ExternalApiError, mock_json):
            RaiseErrorAction.handle(self.response)

    def test_raise_exception_action(self):
        mock_json = 'mock json str'
        self.response.configure_mock(**{"json.return_value": mock_json})

        with self.assertRaisesRegex(ExternalApiException, mock_json):
            RaiseExceptionAction.handle(self.response)

    def test_return_json_or_empty_dict_action(self):
        mock_json = 'mock json str'
        self.response.configure_mock(**{"json.return_value": mock_json})

        result = ReturnJsonOrEmptyDictAction.handle(self.response)

        assert result == mock_json

    def test_return_json_or_error_message_action(self):
        self.response.status_code = HTTPStatus.OK
        self.response.text = 'mock text'
        self.response.url = 'mock url'
        self.response.configure_mock(**{"json.side_effect": ValueError})

        result = ReturnJsonOrErrorMessageAction.handle(self.response)

        expected_err_msg = "Invalid response from External API. " \
                           "Status code:{}. Error: {}.Url: {}".format(
                                self.response.status_code,
                                self.response.text,
                                self.response.url,
                            )

        assert result == expected_err_msg
