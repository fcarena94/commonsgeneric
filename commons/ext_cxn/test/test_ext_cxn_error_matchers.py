from http import HTTPStatus
from unittest import TestCase

import requests

from nwcommons.ext_cxn.matchers.common import RecursiveJsonHttpErrorMatcher


class TestExtCxnErrorMatchers(TestCase):
    def setUp(self):
        super().setUp()
        self.mock_response = requests.Response()
        self.mock_response.status_code = HTTPStatus.OK

    def test_recursive_json_http_error_matcher_matches_correctly(self):
        error_matcher = RecursiveJsonHttpErrorMatcher
        response_json_str = """
        {
            "foo": "bar",
            "baz": {
                "mock": "test",
                "mock2": "test2",
                "numeric": 23
            },
            "status": {
                "error": true,
                "code": 666
            },
            "other_num": 28.12
        }
        """
        self.mock_response._content = bytes(response_json_str, 'utf-8')
        error_to_match = {"status": {"error": True, "code": 666}}
        score = error_matcher.get_match_score(self.mock_response, error_to_match)

        assert score == 1.0

        other_error = {"status": {"error": True, "code": 555}}
        score = error_matcher.get_match_score(self.mock_response, other_error)

        assert score == 0.0

        more_generic_error = {"status": {"error": True}}
        score = error_matcher.get_match_score(self.mock_response, more_generic_error)

        assert score == 1.0

    def test_recursive_json_http_error_matcher_w_bad_response_returns_score_0(self):
        error_matcher = RecursiveJsonHttpErrorMatcher
        self.mock_response._content = None
        error_to_match = {"foo": "bar"}

        score = error_matcher.get_match_score(self.mock_response, error_to_match)

        assert score == 0.0

    def test_recursive_json_http_error_matcher_w_bad_error_returns_score_0(self):
        error_matcher = RecursiveJsonHttpErrorMatcher
        response_json_str = '{"foo":"bar"}'
        self.mock_response._content = bytes(response_json_str, 'utf-8')
        error_to_match = "not_a_dict"

        score = error_matcher.get_match_score(self.mock_response, error_to_match)

        assert score == 0.0