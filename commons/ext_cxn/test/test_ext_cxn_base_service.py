from http import HTTPStatus
from unittest import TestCase
from unittest.mock import Mock

import pytest

from nwcommons.ext_cxn.cxn_base import CxnBaseService
from nwcommons.ext_cxn.base import ErrorDefinition
from nwcommons.ext_cxn.matchers.common import Http200ErrorMatcher
from nwcommons.ext_cxn.actions.common import ReturnJsonOrEmptyDictAction

BASE_SERVICE_PATH = 'commons.ext_cxn.cxn_base'


class TestExtCxnBaseService(TestCase):

    @pytest.fixture(autouse=True)
    def mocker_fix(self, mocker):
        self.mocker = mocker

    def setUp(self):
        super().setUp()

        self.service = CxnBaseService

    def test_base_service_simple_request_success(self):
        generic_errors_chain = [
            ErrorDefinition(
                match_class=Http200ErrorMatcher,
                match_parameters={},
                match_handler=ReturnJsonOrEmptyDictAction,
            )
        ]

        self.service.generic_handlers = generic_errors_chain
        self.service.known_responses = {}

        mock_json = 'mock json str'
        mock_request_retval = Mock()
        mock_request_retval.status_code = HTTPStatus.OK
        mock_request_retval.configure_mock(**{"json.return_value": mock_json})

        self.mocker.patch(
            f'{BASE_SERVICE_PATH}.requests.request',
            return_value=mock_request_retval
        )

        response = self.service.request(
            method='GET',
            url='http://test'
        )

        assert response == {'data': mock_json, 'status_code': HTTPStatus.OK}

    def test_base_service_request_missing_handlers(self):
        self.service.generic_handlers = None

        self.mocker.patch(
            f'{BASE_SERVICE_PATH}.requests.request',
            return_value=Mock()
        )

        expected_err = "'property' object has no attribute 'get'"

        with self.assertRaisesRegex(AttributeError, expected_err):
            self.service.request(method='GET', url='http://test')
