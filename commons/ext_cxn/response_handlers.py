_ACCEPTABLE_MATCH_THRESHOLD = 0.9


class ResponseHandlersUtil:
    """Util class to find the right handler for a given response."""

    @classmethod
    def _get_best_match(cls, response, known_response_handlers):
        best_match = None
        best_score = 0.0
        for handler_definition in known_response_handlers:
            score = handler_definition.match_class.get_match_score(
                response, handler_definition.match_parameters
            )
            if score > best_score:
                best_score = score
                best_match = handler_definition
        return best_score, best_match

    @classmethod
    def find_handler(cls, generic_handlers, known_response_handlers, response):
        """Find an error handler and returns the action class.

        Parameters
        ----------
        known_response_handlers: list
            list for possible error handlers
        response : request.Response
            HTTP response from External API.

        Returns
        -------
        ErrorActionBase
            Subclass of ErrorActionBase that handles the error if found.

        Raises
        ------
        -
        """
        best_score, best_match = cls._get_best_match(
            response, known_response_handlers
        )
        if best_score < _ACCEPTABLE_MATCH_THRESHOLD:
            best_score, best_match = cls._get_best_match(
                response, generic_handlers
            )
        return best_match.match_handler
