from http import HTTPStatus

from commons.api.exceptions import HTTPError


class ExternalApiError(HTTPError):
    """Raised when a request to an External Api throws a 5xx error."""

    @property
    def description(self):
        """Error description."""
        return 'API error.'

    @property
    def code(self):
        """Error code."""
        return 'NW_API_ERRNO_0001'

    @property
    def status_code(self):
        """Error status code."""
        return HTTPStatus.BAD_GATEWAY.value


class ExternalApiException(HTTPError):
    """Raised when a request to an External Api throws a 4xx error."""

    @property
    def description(self):
        """Error description."""
        return 'External Api exception.'

    @property
    def code(self):
        """Error code."""
        return 'NW_API_ERRNO_0002'

    @property
    def status_code(self):
        """Error status code."""
        return HTTPStatus.BAD_REQUEST.value
