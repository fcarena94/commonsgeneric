from abc import ABC, abstractmethod

from nwcommons.logger import get_logger

from . import exceptions  # noqa: I202

logger = get_logger("error_actions")


class ErrorActionBase(ABC):
    """Abstract class that represents error handlers."""

    @classmethod
    @abstractmethod
    def handle(cls, response):
        """
        Handle a response.

        Parameters
        ----------
        response : HTTPResponse
            response to handle
        """
        pass


class RaiseErrorAction(ErrorActionBase):
    """Raise ExternalApiError."""

    @classmethod
    def handle(cls, response):
        """
        Raise ExternalApiError.

        Parameters
        ----------
        response : HTTPResponse
            response to handle

        Raises
        ------
        ExternalApiError
            Raised when the response is not a valid JSON.
        """
        json_data = None
        try:
            json_data = response.json()
        except ValueError:
            pass

        logger.info(
            "Received a new response: %s. Status Code: %s. Url: %s",
            str(response), response.status_code, response.url,
        )
        raise exceptions.ExternalApiError(json_data)


class RaiseExceptionAction(ErrorActionBase):
    """Raise an ExternalApiException."""

    @classmethod
    def handle(cls, response):
        """
        Raise ExternalApiException.

        Parameters
        ----------
        response : HTTPResponse
            response to handle

        Raises
        ------
        ExternalApiException
            Raised when the response is not a valid JSON.
        """
        json_data = None
        try:
            json_data = response.json()
        except ValueError:
            pass

        logger.info(
            "Received a new response: %s. Status Code: %s. Url: %s",
            str(response), response.status_code, response.url,
        )
        raise exceptions.ExternalApiException(json_data)


class ReturnJsonOrEmptyDictAction(ErrorActionBase):
    """Try to parse a json, and return an empty dict if it's invalid."""

    @classmethod
    def handle(cls, response):
        """
        Parse json or return an empty dict.

        Parameters
        ----------
        response : HTTPResponse
            response to handle

        Returns
        -------
        dict
            Parsed response as a dict, or an empty dict.

        Raises
        ------
        -
        """
        try:
            json_data = response.json()
        except ValueError:
            json_data = {}
        logger.info(
            "Received a new response for External API: {}. \
                        Status Code: {}. Url: {}".format(
                str(response), response.status_code, response.url
            )
        )
        return json_data


class ReturnJsonOrErrorMessageAction(ErrorActionBase):
    """Try to parse a json or return an error message if it's invalid."""

    @classmethod
    def handle(cls, response):
        """
        Parse json or return a dict or a str with an error message.

        Parameters
        ----------
        response : HTTPResponse
            response to handle

        Returns
        -------
        dict or str
            Parsed response as a dict, or dict with an error message.

        Raises
        ------
        -
        """
        json_data = None
        try:
            json_data = response.json()
        except ValueError:
            message = "Invalid response from External API. Status code:"\
                f"{response.status_code}. Error: {response.text}."\
                f"Url: {response.url}"
            return message
        else:
            logger.info(
                f"Received a new response for External API: {json_data}."
                f"Status Code: {response.status_code}."
                f"Url: {response.url}"
            )
            return json_data
