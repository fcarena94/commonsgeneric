from abc import ABC, abstractmethod
from datetime import datetime, timezone
from functools import wraps

import requests

from nwcommons.ext_cxn.actions.exceptions import ExternalApiError
from nwcommons.ext_cxn.response_handlers import ResponseHandlersUtil
from nwcommons.logger import get_logger

logger = get_logger("cxn_library")


def handle_requests_error(method):
    """Translate all requests errors to ExternalApiError.

    Parameters
    ----------
    method : method
        method to decorate.

    Returns
    -------
    func
        method actual implementation.

    Raises
    ------
    ExternalApiError
        if any RequestException is raised.
    """

    @wraps(method)
    def _impl(*method_args, **method_kwargs):
        try:
            result = method(*method_args, **method_kwargs)
        except requests.exceptions.RequestException as e:
            logger.debug(f"request err: {e}")
            raise ExternalApiError(str(e))
        else:
            return result

    return _impl


class CxnBaseService(ABC):
    """Base Class for any Service that connects to External Api."""

    @property
    @classmethod
    @abstractmethod
    def known_responses(cls):
        """Map of response handlers and definitions.

        Help define the behavior of each response according to the specific
        service definition and status_code.
        The map is composed of http_code as keys and list of possible response
        handlers definitions for this service as values.
        """
        pass

    @property
    @classmethod
    @abstractmethod
    def generic_handlers(cls):
        """Override this method with specific generic Error Definitions."""
        pass

    @classmethod
    def _get_headers(cls):
        """
        Override this method with specific headers.

        ...

        Returns
        -------
        dict
            empty dict, should be override when needed custom headers.
        """
        return {}

    @classmethod
    def _get_formatted_timestamp(cls):
        """Return datetime.now.

        Tz aware and formatted as required by External API
        """
        return datetime.now(timezone.utc).isoformat()[:-6] + "Z"

    @classmethod
    @handle_requests_error
    def request(cls, method, url, data=None, extra_headers=None):
        """Make and execute http request.

        Parameters
        ----------
        method : str
            http method.
        url : str
            destination url.
        data : dict, optional
            request body data (None by default).
        extra_headers : dict, optional
            extra items to add to headers (None by default)

        Returns
        -------
        dict
            response data from request.
        """
        api_name = cls.__name__
        headers = cls._get_headers()

        if isinstance(extra_headers, dict):
            headers.update(extra_headers)

        message = (
            "Calling from {} . URL: {}. Method: {}. Headers: {}. Payload: {}"
        ).format(api_name, url, method, headers, data)

        logger.debug(message)

        response = requests.request(
            headers=headers, method=method, json=data, url=url, verify=False
        )

        json_data = cls.handle_response(response)

        return {"data": json_data, "status_code": response.status_code}

    @classmethod
    def handle_response(cls, response):
        """Handle External API response.

        Parameters
        ----------
        response : request.Response
            HTTP response from External API.

        Returns
        -------
        dict
            json response from External API.

        Raises
        ------
        ExternalAPIError
            if External API response status code is in the range of 5xx.
        ExternalAPIException
            if External API response status code is in the range of 4xx.
        """
        specific_handlers = cls.known_responses.get(response.status_code, [])
        response_handler = ResponseHandlersUtil.find_handler(
            cls.generic_handlers, specific_handlers, response
        )
        json_data = response_handler.handle(response)

        return json_data
