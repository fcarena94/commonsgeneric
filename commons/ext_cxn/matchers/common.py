from abc import ABC, abstractmethod

from deepdiff import DeepDiff


def _rough_string_similarity_score(str1, str2):
    """
    Return a similarity score for two strings.

    If one string is contained in the other, then
    we return a value 0.0-1.0 that represents how much they match.

    Parameters
    ----------
    str1 : str
        String to compare
    str2 : str
        String to compare

    Returns
    -------
    float
        Value between 1.0 and 0.0 that represents the similarity.
    """
    if len(str1) > len(str2):
        str_a = str1
        str_b = str2
    else:
        str_a = str2
        str_b = str1

    return len(str_b) / len(str_a) if str_b in str_a else 0.0


def _dict_contains(parent_dict, subdict, **kwargs):
    """Check if a parent dictionary contains a sub dictionary.

    The DeepDiff() method returns a dict of the difference between two dicts.
    If the key 'values_changed' or 'dictionary_item_added' is present, it
    means that the subdict is not present in the parent_dict (or it is
    different).

    Parameters
    ----------
    parent_dict : dict
        The dict in which to search
    subdict : str
        The dict to search in the parent dict

    Returns
    -------
    boolean
        Whether the parent dict contains the subdict or not.
    """
    if not isinstance(subdict, dict):
        return False

    diff = DeepDiff(parent_dict, subdict, **kwargs)

    if diff.get('values_changed') or diff.get('dictionary_item_added'):
        return False
    return True


class ErrorMatcherBase(ABC):
    """
    Abstract class to match responses to their handlers.

    Return a float match score from 0.0 to 1.0 representing
    how the `response` and the `known_error` are related
    """

    @classmethod
    @abstractmethod
    def get_match_score(cls, response, known_error):
        """
        Implement this on subclasses.

        Parameters
        ----------
        response : dict
            validated data to update into the user record.
        known_error: ErrorDefinition
            knwon error definition from each service.

        Returns
        -------
        float
            Score from 0.0 to 1.0
        """
        pass


class JsonHttpErrorMatcher(ErrorMatcherBase):
    """Tries to match a JSON response with a status."""

    @classmethod
    def get_match_score(cls, response, known_error):
        """
        Return a value from 0.0 to 1.0 depending on the level of similarity.

        Partial strings in payloads also work fine.
        So if you have a string like this:
        'Specific error for user (some user)' you can define an error with
        a message like 'Specific error for user ', and the end score
        will be good enough to produce a match.

        Parameters
        ----------
        response : dict
            validated data to update into the user record.
        known_error: ErrorDefinition
            knwon error definition from each service.

        Returns
        -------
        float
            Score from 0.0 to 1.0
        """
        final_score = 0.0
        score_unit = 1 / len(known_error.keys())
        try:
            json_data = response.json()
        except ValueError:
            pass
        else:
            for key, known_value in known_error.items():
                if key in json_data:
                    if json_data[key] == known_value:
                        final_score += score_unit
                    else:
                        final_score += (
                            score_unit * _rough_string_similarity_score(
                                str(json_data[key]), str(known_value)
                            )
                        )
        return final_score


class RecursiveJsonHttpErrorMatcher(ErrorMatcherBase):
    """Tries to recursively match a nested JSON response with a status."""

    @classmethod
    def get_match_score(cls, response, known_error, **kwargs):
        """
        Return 1.0 if the expected error is present in the response.

        known_error is expected to be a dict.

        Parameters
        ----------
        response : dict
            validated data.
        known_error: ErrorDefinition
            knwon error definition from each service.

        Returns
        -------
        float
            Score: Either 0.0 or 1.0
        """
        error_is_present = False
        try:
            json_data = response.json()
        except ValueError:
            pass
        else:
            error_is_present = _dict_contains(json_data, known_error, **kwargs)
        return (1.0 if error_is_present else 0.0)


class Http400To499ErrorMatcher(ErrorMatcherBase):
    """Tries to match a response with a status code."""

    @classmethod
    def get_match_score(cls, response, known_error):
        """
        Return 1.0 if the status code is >=400 and <500, 0.0 otherwise.

        Parameters
        ----------
        response : dict
            validated data to update into the user record.
        known_error: ErrorDefinition
            knwon error definition from each service.

        Returns
        -------
        float
            Score from 0.0 to 1.0
        """
        return (
            1.0
            if response.status_code >= 400 and response.status_code < 500
            else 0.0
        )


class Http500OrAboveErrorMatcher(ErrorMatcherBase):
    """
    Try to match a response with a status code.

    Returns 1.0 if the status code is >=500, 0.0 otherwise
    """

    @classmethod
    def get_match_score(cls, response, known_error):
        """
        Return 1.0 if the status code is >=500, 0.0 otherwise.

        Parameters
        ----------
        response : dict
            validated data to update into the user record.
        known_error: ErrorDefinition
            knwon error definition from each service.

        Returns
        -------
        float
            Score from 0.0 to 1.0
        """
        return 1.0 if response.status_code >= 500 else 0.0


class Http200ErrorMatcher(ErrorMatcherBase):
    """
    Try to match a response with a status code.

    Returns 1.0 if the status code is 200, 0.0 otherwise
    """

    @classmethod
    def get_match_score(cls, response, known_error):
        """
        Return 1.0 if the status code is ==200, 0.0 otherwise.

        Parameters
        ----------
        response : dict
            validated data to update into the user record.
        known_error: ErrorDefinition
            knwon error definition from each service.

        Returns
        -------
        float
            Score from 0.0 to 1.0
        """
        return 1.0 if response.status_code == 200 else 0.0


class FallbackErrorMatcher(ErrorMatcherBase):
    """
    Fallback matcher. Return 1.0 at the end of the matchers chain.

    After all other matchers failed to match, this one matches all unhandled
    responses to make sure they are not skipped.
    """

    @classmethod
    def get_match_score(cls, response, known_error):
        """
        Return 1.0 always.

        Parameters
        ----------
        response : dict
            validated data to update into the user record.
        known_error: ErrorDefinition
            knwon error definition from each service.

        Returns
        -------
        float
            Score from 0.0 to 1.0
        """
        return 1.0
