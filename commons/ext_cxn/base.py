from collections import namedtuple


ErrorDefinition = namedtuple(
    'ErrorDefinition', ['match_class', 'match_parameters', 'match_handler']
)
