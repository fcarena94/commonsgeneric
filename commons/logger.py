from datetime import datetime
import json
import logging
from os import environ

from pythonjsonlogger.jsonlogger import JsonFormatter
import rollbar
from rollbar.logger import RollbarHandler

log_format = '(timestamp) (level) (name) (pathname) (lineno) (funcName) (' \
             'message)'

SECRET_PARAMS = {
    "pin": lambda x: '#' * len(x),
    "password": lambda x: '#' * len(x),
    "new_password": lambda x: '#' * len(x),
    "current_password": lambda x: '#' * len(x),
    "current_pin": lambda x: '#' * len(x),
    "new_pin": lambda x: '#' * len(x),
    "number": lambda x: f"{str(x)[:4]}{'#' * 8}{str(x)[12:]}",
    "card_number": lambda x: f"{str(x)[:4]}{'#' * 8}{str(x)[12:]}",
    "cardNumber": lambda x: f"{str(x)[:4]}{'#' * 8}{str(x)[12:]}",
    # card digits from LMB
    "cvc": lambda x: "#" * len(x)
}

AVOID_BIG_CONTENT_PARAMS = [
    'ocr',
    'selfie',
    'face_id',
    'base64_front',
    'base64_back',
    'base64_selfie',
    'file_content',
    'selfieList',
    'phone_contacts',
    'phone_numbers'
]


def big_content(x):
    """Return constant for replacement of big content logs."""
    return "--big-content--"


class CustomJsonFormatter(JsonFormatter):
    """Format logs as JSON."""

    def add_fields(self, log_record, record, message_dict):
        """Add fields to a log message.

        Parameters
        ----------
        log_record: dict
            raw log data.

        record: str
            level of the log

        message_dict: dict
            message to be logged
        """
        super(CustomJsonFormatter, self).add_fields(
            log_record, record, message_dict
        )
        if not log_record.get('timestamp'):
            # this doesn't use record.created, so it is slightly off
            now = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%fZ')
            log_record['timestamp'] = now
        if log_record.get('level'):
            log_record['level'] = log_record['level'].upper()
        else:
            log_record['level'] = record.levelname
        if log_record.get("headers", {}).get("X-Request-ID", None) is not None:
            log_record["request_id"] = log_record.get("headers").get(
                "X-Request-ID"
            )

    def process_log_record(self, log_record):
        """Implement custom logic to replace sensitive data.

        Parameters
        ----------
        log_record : dict
            raw log data.

        Returns
        -------
        dict
            logs with sensible data replaced.
        """
        for key in SECRET_PARAMS:
            log_record = self.replace_param(key,
                                            log_record, SECRET_PARAMS[key])

        for key in AVOID_BIG_CONTENT_PARAMS:
            log_record = self.replace_param(key, log_record,
                                            big_content)

        return log_record

    def replace_param(self, needle, haystack, new_value):
        """Search and replace a needle's value from a haystack.

        If is possible to convert the needle's value as a json object,
        evaluates it recursively.

        Parameters
        ----------
        needle : str
            param name to match.
        haystack : dict
            data to search the param
        new_value: str
            value to replace the original one
        Returns
        -------
        dict
            haystack with replaced data.
        """
        for key in haystack:
            param = haystack.get(key)
            if isinstance(param, dict):
                self.replace_param(needle, param, new_value)
            elif isinstance(param, str):
                try:
                    param = json.loads(param)
                except Exception:
                    pass
                else:
                    try:
                        self.replace_param(needle, param, new_value)
                    except TypeError:
                        pass
                    else:
                        haystack[key] = param
            if key == needle:
                haystack[key] = new_value(str(haystack[key]))

        return haystack


def get_logger(name="general_logger", file_path=None):
    """Create logger for given name if does not exist.

    Also add handler and formatter. If LOG_LEVEL env variable is set
    configure logger.level with  that Log Level.
    Log level options are:
        CRITICAL, ERROR, WARNING, INFO, DEBUG, NOTSET

    Parameters
    ----------
    name : str
        The log name. Logically, represents the application that used the
        logger
    file_path : str
        The log file path
    """
    logger = logging.getLogger(name)

    selected_level = logging.DEBUG
    for level in logging._levelToName.items():
        if level[1] == environ.get("LOG_LEVEL"):
            selected_level = level[0]

    logger.setLevel(selected_level)

    formatter = CustomJsonFormatter(log_format)

    if logger.handlers == []:
        log_handler = logging.StreamHandler()
        log_handler.setFormatter(formatter)
        logger.addHandler(log_handler)

        if file_path is not None:
            file_handler = logging.FileHandler(file_path)
            file_handler.setFormatter(formatter)
            logger.addHandler(file_handler)

        add_rollbar_handler(logger)

    return logger


def add_rollbar_handler(current_logger):
    """Add to an already created logger the handler to log on Rollbar.

    Parameters
    ----------
    current_logger
        an already created python logger.
    """
    token = environ.get("ROLLBAR_TOKEN")
    environment = environ.get("ROLLBAR_ENVIRONMENT")

    if token is not None and environment is not None:
        rollbar.init(token, environment)

        rollbar_handler = RollbarHandler(history_size=3)
        rollbar_handler.setLevel(logging.WARNING)

        rollbar_handler.setHistoryLevel(logging.DEBUG)

        current_logger.addHandler(rollbar_handler)
