from os import environ

from celery import Celery


def flask_context_celery(app):
    """Set flask context for celery."""
    if any([
        environ.get('CELERY_BACKEND') is None,
        environ.get('CELERY_BROKER') is None
    ]):
        raise Exception(
            "Env vars missing: CELERY_BACKEND or CELERY_BROKER"
        )

    backend = environ.get("CELERY_BACKEND")
    broker = environ.get("CELERY_BROKER")
    celery = Celery(app.import_name, broker=broker)
    celery.conf.result_backend = backend

    BaseTask = celery.Task

    class ContextTask(BaseTask):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return BaseTask.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery
