import re

from nwcommons.constants import (
    AREA_CODES, COUNTRY_CODE, MOBILE_PREFIX, PHONE_LENGTH
)


class E164PhoneFormatter:
    """Format a phone number."""

    e164_format = "+{country}{prefix}{area_code}{number}"

    @classmethod
    def parse(cls, phone):
        """Format a phone number into its different components.

        Parameters
        ----------
        phone : str
            represents a phone number.

        Returns
        -------
        dict
            dictionary with 'area' and 'number' components from phone.
        """
        result_value = {}

        country_regex = "((00{country})|([+]{country}))".format(
            country=COUNTRY_CODE
        )
        national_mobile_prefix = "({prefix})".format(
            prefix=MOBILE_PREFIX
        )
        number_prefix = "(0)"
        mobile_prefix = "(15)"

        for code in AREA_CODES:
            number_len = PHONE_LENGTH - len(str(code))

            area_regex = "(?P<area>{area_code})".format(area_code=code)
            number_regex = "(?P<number>[0-9]{{{len}}})".format(len=number_len)

            str_regex = r"^{country}?{national_mobile}?{prefix}?{area_code}" \
                r"?{mobile_prefix}?{number}$".format(
                    country=country_regex,
                    national_mobile=national_mobile_prefix,
                    prefix=number_prefix,
                    area_code=area_regex,
                    mobile_prefix=mobile_prefix,
                    number=number_regex
                )

            result = re.search(str_regex, phone)

            if result is not None:
                result_value['area'] = result.group('area')
                result_value['number'] = result.group('number')
                break

        return result_value

    @classmethod
    def _is_valid_number_for_area(cls, number, area):
        """Validate if the len of a number is valid for the area.

        Parameters
        ----------
        area : str
            code area representation.
        number : str
            number to be validated.

        Returns
        -------
        boolean
            True if is valid, else False.
        """
        return (len(area) + len(number)) == PHONE_LENGTH

    @classmethod
    def format(cls, raw_phone_number, base_phone_number):
        """Format phone_param into a phone number in E164 format.

        Parameters
        ----------
        raw_phone_number : str
            phone number to be formatted.
        base_phone_number : str
            phone number to replace area code if it is missing in phone_param.

        Returns
        -------
        str
            phone number formatted in E164 format, or None.
        """
        phone = re.sub("[^+0-9]", "", raw_phone_number)
        base_phone = re.sub("[^+0-9]", "", base_phone_number)

        formatted_phone = cls.parse(phone)

        area = formatted_phone.get('area')
        if area is None:
            area = cls.parse(base_phone).get('area', '')

        phone_number = formatted_phone.get('number', '')
        if cls._is_valid_number_for_area(phone_number, area):
            return cls.e164_format.format(
                country=COUNTRY_CODE,
                prefix=MOBILE_PREFIX,
                area_code=area,
                number=formatted_phone.get('number')
            )

        return None
