def format_to_pesos(amount):
    """Format amount.

    Transform amount from cent to pesos and format it to use a comma for
    thousand separator and a dot for decimal separator.
    Translate uses ascii code to map chars.
    44 : ' , '
    46 : ' . '
    120: ' x '
    """
    pesos = int(amount) / 100

    return "{:,.2f}".format(pesos).\
        translate({44: 46, 46: 44})
