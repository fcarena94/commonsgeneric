from nwcommons.helpers import phone_formatter
from unittest import TestCase


class TestRegisterSchemas(TestCase):

    def setUp(self):
        super().setUp()
        self.formatter = phone_formatter.E164PhoneFormatter

        self.base_phone = "+5491187654321"

    def test_format_invalid_phone_returns_none(self):
        invalid_phone = "some_phone"
        formatted = self.formatter.format(invalid_phone, self.base_phone)

        assert formatted is None

    def test_format_invalid_number_for_area_returns_none(self):
        # phone with area code '11'
        phone_with_other_area = "+5491187654321"
        # valid number for area codes with 3 digits
        valid_phone = "15123456789"
        formatted = self.formatter.format(valid_phone, phone_with_other_area)

        assert formatted is None

    def test_format_e164_format_phone_returns_it(self):
        phone = "+5491187654321"

        formatted = self.formatter.format(phone, self.base_phone)

        assert formatted == phone

    def test_format_valid_number_with_00_returns_formatted(self):
        phone = "005491187654321"
        expected_phone = "+5491187654321"

        formatted = self.formatter.format(phone, self.base_phone)

        assert formatted == expected_phone

    def test_format_e164_format_phone_with_different_base_area_returns_it(self):
        base_phone = "+5491187654321"
        phone = "+5492901654321"

        formatted = self.formatter.format(phone, base_phone)

        assert formatted == phone

    def test_format_valid_number_without_country_returns_formatted(self):
        phone = "0111587654321"
        expected_phone = "+5491187654321"

        formatted = self.formatter.format(phone, self.base_phone)

        assert formatted == expected_phone

    def test_format_valid_number_without_country_nor_mobile_prefix(self):
        phone = "2901654321"
        expected_phone = "+5492901654321"

        formatted = self.formatter.format(phone, self.base_phone)

        assert formatted == expected_phone

    def test_format_valid_number_for_base_phone_returns_formatted(self):
        base_phone = "+5491112345678"
        phone = "1587654321"
        expected_phone = "+5491187654321"

        formatted = self.formatter.format(phone, base_phone)

        assert formatted == expected_phone

    def test_format_numbers_with_valid_characters(self):
        valid_numbers_with_characters = [
            "+54 (911) 8765-4321",
            "00 54 (911) 8765-4321",
            "+54 (2901) 65-4321",
            "(011) 8765-4321",
            "2901-65-4321",
            "223-765-4321"
        ]

        for number in valid_numbers_with_characters:
            if self.formatter.format(number, self.base_phone) is None:
                assert False

        assert True

    def test_parse_numbers_from_some_different_area_codes_parse_successfully(self):
        some_numbers = [
            "+5493416943454",
            "+5492477311584",
            "+5491141311584",
            "+5490223123123"
        ]

        for number in some_numbers:
            if self.formatter.parse(number) is {}:
                assert False

        assert True
