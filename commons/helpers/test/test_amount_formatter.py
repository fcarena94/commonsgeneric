from unittest import TestCase

from nwcommons.helpers import amount_formatter


class TestAmountFormatter(TestCase):

    def test_formatter(self):
        amount = 1000099
        expected_amount = '10.000,99'
        formatted_amount = amount_formatter.format_to_pesos(amount)

        assert expected_amount == formatted_amount


