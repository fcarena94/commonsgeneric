from os import environ
from unittest import TestCase

import pytest

from nwcommons.helpers.cache_db import (
    DataDecoder,
    get_cache_db,
    start_cache_database,
    CacheDatabaseProxy
)

CACHE_DB_PATH = 'commons.helpers'


class TestCacheDatabase(TestCase):

    def setUp(self):
        self.CACHE_DATABASE_TEST_URL="redis://@localhost:6379/1"
        self.db = get_cache_db()
        if self.db is None:
            self.db = start_cache_database(
                self.CACHE_DATABASE_TEST_URL
            )

    @pytest.fixture(autouse=True)
    def mocker_fix(self, mocker):
        self.mocker = mocker

    def test_connection_to_redis(self):
        self.mocker.patch(CACHE_DB_PATH + '.cache_db.CacheDatabaseProxy.ping')
        db = start_cache_database(self.CACHE_DATABASE_TEST_URL)
        assert isinstance(db, CacheDatabaseProxy)

    def test_connection_to_redis_fails_with_wrong_parameters(self):
        with pytest.raises(Exception) as err:
            start_cache_database('bad_connection_url')

        expected_error = 'Redis URL must specify one of the ' \
                         'followingschemes (redis://, rediss://, unix://)'

        assert expected_error == err.value.args[0]

    def test_get_unexisting_value(self):
        value = self.db.get("not_existing_key")

        assert value is None

    def test_integration_get_set_and_delete_commands(self):
        test_key = 'test'
        test_value = 'test_value'
        self.db.set(test_key, test_value)

        value = self.db.get(test_key, decoder=DataDecoder.STR)
        assert test_value == value

        amount = self.db.delete(test_key)
        assert 1 == amount

    def test_integration_flush_db(self):
        test_key = 'test'
        test_value = 'test_value'

        self.db.set(test_key, test_value)
        self.db.flush_db()
        amount = self.db.delete(test_key)

        assert 0 == amount

    def test_str_decoder(self):
        byte_str = bytes('test', encoding='utf-8')
        assert 'test' == DataDecoder.STR(byte_str)

    def test_int_decoder(self):
        byte_int = bytes("1", encoding='utf-8')
        assert 1 == DataDecoder.INT(byte_int)

    def test_json_decoder(self):
        byte_json = bytes('{"test":"test_value"}', encoding='utf-8')
        assert {"test": "test_value"} == DataDecoder.JSON(byte_json)
