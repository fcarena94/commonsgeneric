import pytest


from nwcommons.helpers.encrypting import (
    BcryptCipher
)
from nwcommons.helpers.encrypting.exceptions import (
    InvalidPasswordError
)
from nwcommons.testing import BaseTest


class TestLoginCredentialsModel(BaseTest):

    def test_password_hashed_is_not_like_password_input(self):
        plain_password = "someplaintext"
        hashed_password = BcryptCipher.hash_password(plain_password)

        assert hashed_password is not None
        assert plain_password != hashed_password

    def test_password_hashed_result_return_string_value(self):
        plain_password = "someplaintext"
        hashed_password = BcryptCipher.hash_password(plain_password)

        assert isinstance(hashed_password, str)

    def test_password_hashed_match_plain_text_password(self):
        plain_password = "someplaintext"
        hashed_password = BcryptCipher.hash_password(plain_password)

        assert BcryptCipher.check_password(plain_password, hashed_password)

    def test_password_hashed_that_not_match_password_return_false(self):
        plain_password = "someplaintext"
        hashed_password = BcryptCipher.hash_password(plain_password)
        wrong_password = "wrongpassword"

        assert not BcryptCipher.check_password(wrong_password, hashed_password)

    def test_hash_password_beyond_72_chars_raise_error(self):
        plain_password = "some_text_" * 7
        plain_password = plain_password + "123"
        with pytest.raises(InvalidPasswordError):
            BcryptCipher.hash_password(plain_password)
