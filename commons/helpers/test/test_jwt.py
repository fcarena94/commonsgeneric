import time
from unittest import TestCase

import pytest

from nwcommons.helpers.jwt import JwtManager
from nwcommons.helpers.jwt import exceptions
from nwcommons.helpers.jwt.jwt import reserved_claims


class TestJWT(TestCase):

    def test_jwt_encoding_ok(self):
        secret = 'secret_key'
        hours = 1
        payload = {
            "test": "result"
        }

        token = JwtManager.create_jwt(payload, secret, hours)

        assert token is not None
        assert type(token) is str

    def test_jwt_encoding_with_error(self):
        secret = 33               # Not a string
        hours = 1
        payload = {
            "test": "result"
        }

        with pytest.raises(exceptions.JwtError):
            JwtManager.create_jwt(payload, secret, hours)

    def test_jwt_decoding(self):
        secret = 'secret_key'
        hours = 1
        original_payload = {
            "test": "result"
        }

        token = JwtManager.create_jwt(original_payload, secret, hours)
        decoded_payload = JwtManager.decode_jwt(token, secret)

        assert 'test' in decoded_payload
        assert decoded_payload['test'] == 'result'
        assert not any(key in decoded_payload for key in reserved_claims)

    def test_jwt_expires_after_expiration_time(self):
        secret = 'secret_key'
        hours = 0.001               # 3.6 seconds
        original_payload = {
            "test": "result"
        }

        token = JwtManager.create_jwt(original_payload, secret, hours)

        decoded_payload = JwtManager.decode_jwt(token, secret)
        assert decoded_payload is not None

        time.sleep(5)

        with pytest.raises(exceptions.JwtExpiredSignatureError):
            JwtManager.decode_jwt(token, secret)

    def test_jwt_raises_error_with_invalid_secret_key(self):
        secret = 'secret_key'
        hours = 1
        original_payload = {
            "test": "result"
        }

        token = JwtManager.create_jwt(original_payload, secret, hours)

        with pytest.raises(exceptions.JwtInvalidSignatureError):
            JwtManager.decode_jwt(token, 'invalid_secret_key')

    def test_jwt_with_no_expiration(self):
        secret = 'secret_key'
        original_payload = {
            "test": "result"
        }

        token = JwtManager.create_jwt(original_payload, secret)
        decoded_payload = JwtManager.decode_jwt(token, secret)

        assert 'test' in decoded_payload
        assert decoded_payload['test'] == 'result'
        assert not any(key in decoded_payload for key in reserved_claims)

    def test_jwt_token_is_different_in_short_generation_interval(self):
        secret = 'secret_key'
        hours = 1
        original_payload = {
            "test": "result"
        }

        token1 = JwtManager.create_jwt(original_payload, secret, hours)
        token2 = JwtManager.create_jwt(original_payload, secret, hours)

        assert token1 != token2
