from os import environ
from unittest import TestCase

from flask import Flask

from nwcommons.helpers import (
    celery,
    date
)


class TestEnvVars(TestCase):

    def test_celery_backend_not_set_raises_error(self):
        environ.clear()
        environ['CELERY_BROKER'] = 'KEY'

        with self.assertRaises(Exception) as error:
            celery.flask_context_celery(Flask(__name__))

        expected_ex = Exception(
            "Env vars missing: CELERY_BACKEND or CELERY_BROKER"
        )

        assert (str(error.exception) == str(expected_ex))

    def test_celery_broker_not_set_raises_error(self):
        environ.clear()
        environ['CELERY_BACKEND'] = 'KEY'

        with self.assertRaises(Exception) as error:
            celery.flask_context_celery(Flask(__name__))

        expected_ex = Exception(
            "Env vars missing: CELERY_BACKEND or CELERY_BROKER"
        )

        assert (str(error.exception) == str(expected_ex))

    def test_celery_works_with_all_env_vars(self):
        environ['CELERY_BACKEND'] = 'KEY'
        environ['CELERY_BROKER'] = 'KEY'

        try:
            celery.flask_context_celery(Flask(__name__))
        except Exception:
            self.fail("flask_context_celery() raised an Exception")

    def test_timezone_not_set_raises_error(self):
        with self.assertRaises(Exception) as error:
            date.now()

        expected_ex = Exception("The env var 'TIMEZONE' is missing")
        assert (str(error.exception) == str(expected_ex))
