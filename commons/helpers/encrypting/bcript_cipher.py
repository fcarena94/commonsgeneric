import bcrypt


from nwcommons.helpers.encrypting.exceptions import (
    InvalidPasswordError
)

MAX_PASSWORD_LENGTH = 72


class BcryptCipher:
    """Class to hash and check passwords with bcrypt library."""

    @staticmethod
    def hash_password(password):
        """Allow hashing password with bcrypt algorithim.

        Parameters
        ----------
        password : str
            password to be hashed.

        Returns
        -------
        str
            hashed password.

        Raises
        ------
        InvalidPasswordError
            raised if password size is beyond 72 characters .
        """
        if len(password) > MAX_PASSWORD_LENGTH:
            raise InvalidPasswordError("Password too long.")
        password_in_bytes = bytes(password, 'utf-8')

        hash_result = bcrypt.hashpw(password_in_bytes, bcrypt.gensalt())

        return hash_result.decode("utf-8")

    @staticmethod
    def check_password(password, password_hash):
        """Check if password match some hashed before.

        Parameters
        ----------
        password : str
            plain password.
        password_hash : str
            hash to check if match password.

        Returns
        -------
        boolean
            true if password match
        """
        password_in_bytes = bytes(password, 'utf-8')
        password_hash_in_bytes = bytes(password_hash, 'utf-8')
        is_valid = bcrypt.checkpw(password_in_bytes, password_hash_in_bytes)

        return is_valid
