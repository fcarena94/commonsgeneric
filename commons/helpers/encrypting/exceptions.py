class InvalidPasswordError(Exception):
    """Raised when an error on password has ocurred."""
