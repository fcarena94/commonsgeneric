from enum import Enum
import json

import redis

CACHE_DB = None


def get_cache_db():
    """Return the global CacheDatabaseProxy instance.

    Returns
    ------
    CacheDatabaseProxy
        CacheDatabaseProxy instance.
    """
    return CACHE_DB


def start_cache_database(url):
    """Create the global CacheDatabaseProxy instance.

    Creates a BlockingConnectionPool that will be used by the database
    connection. Ping the database to make sure the connection is stablished
    and returns the connection instance

    Parameters
    ----------
    url : str
        Cache database url connection string.

    Returns
    ------
    CacheDatabaseProxy
        CacheDatabaseProxy instance.
    """
    global CACHE_DB
    connection_pool = redis.BlockingConnectionPool.from_url(url)
    connection = redis.Redis(connection_pool=connection_pool)
    cache_db = CacheDatabaseProxy(connection)
    try:
        cache_db.ping()
    except redis.ConnectionError as err:
        raise Exception("Couldn't connect to cache database: {}".format(err))
    else:
        CACHE_DB = cache_db

    return CACHE_DB


class DataDecoder(Enum):
    """Represent the possible data type decoders."""

    STR = (lambda x: x.decode("utf-8"))
    INT = (lambda x: int(x))
    BYTES = (lambda x: bytes(x))
    JSON = (lambda x: json.loads(x))


class CacheDatabaseProxy:
    """Class for creating connection with the cache database."""

    def __init__(self, connection):
        """Create the connection with the cache database.

        Creates a BlockingConnectionPool that will be used by the connection.

        Parameters
        ----------
        connection : Redis connection
            Redis connection to DB.
        """
        self.__cxn = connection

    def ping(self):
        """Ping database."""
        return self.__cxn.ping()

    def get(self, key, decoder=DataDecoder.BYTES):
        """Get the value associated with the provided key.

        Parameters
        ----------
        key : str
            Key name.
        decoder : DataDecoder
            DataDecoder enum instance.

        Returns
        ------
        bytes or None
            Key value in bytes if key exists or None if not.
        """
        result = self.__cxn.get(key)
        return self.__apply_decoder(result, decoder)

    def set(self, key, value, expiration=None, not_exist=False, exists=False):
        """Set a new key with the specified value.

        Parameters
        ----------
        key : str
            Key name.
        value : str, int or bytes
            Key name.
        expiration : int
            Key expiration time in seconds.
        not_exist : bool
            If set to True, set the value at 'key' to 'value' only if it
            does not exist.
        exists : bool
            If set to True, set the value at 'key' to 'value' only if it
            already exists
        """
        self.__cxn.set(
            name=key,
            value=value,
            ex=expiration,
            nx=not_exist,
            xx=exists
        )

    def delete(self, *keys):
        """Delete the provided keys from redis.

        Parameters
        ----------
        *keys
            Key names.

        Returns
        ------
        int
            Number of deleted keys.
        """
        return self.__cxn.delete(*keys)

    def flush_db(self):
        """Delete all keys from current redis database."""
        self.__cxn.flushdb()

    @classmethod
    def __apply_decoder(cls, value, decoder):
        """Decode the provided value with the specified encoder.

        Parameters
        ----------
        value : bytes
            value.
        decoder : func
            decoding function.

        Returns
        ------
        decoded value or None
            decoded value if value is not None, else None.
        """
        if value is not None:
            value = decoder(value)

        return value
