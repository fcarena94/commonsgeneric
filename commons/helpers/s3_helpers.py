import os


def s3_url_for(filename):
    """Return absolute url for assets in S3 storage.

    Append S3 url to filename. S3_URL must be declared on envvars.

    Parameters
    ----------
    filename : str
        file relative url.

    Returns
    -------
    dirc
        url with s3 file in S3
    """
    s3_url = os.environ.get('S3_URL')
    return dict(s3_url_for=f'{s3_url}{filename}')


def s3_url_for_str(filename):
    """Return absolute url for assets in S3 storage.

    Append S3 url to filename. S3_URL must be declared on envvars.

    Parameters
    ----------
    filename : str
        file relative url.

    Returns
    -------
    str
        url with s3 file in S3
    """
    s3_url = os.environ.get('S3_URL')
    return f'{s3_url}{filename}'
