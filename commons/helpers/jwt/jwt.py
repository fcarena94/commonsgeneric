import datetime

import jwt

from . import exceptions

algorithm = 'HS256'
reserved_claims = ['exp', 'nbf', 'iat', 'iss', 'aud', 'timestamp']


class JwtManager:
    """Manage Jwt tokens."""

    @staticmethod
    def create_jwt(payload, secret, expiration_hours=0):
        """Create JWT using the provided payload and secret key.

        If expiration hours is 0, the token won't expire.

        Parameters
        ----------
        payload : dict
            The JWT payload.
        secret : str
            The secret string key used for encoding the token.
        expiration_hours : int
            The hours after the token expires. 0 indicates that the token
            don't expire

        Returns
        -------
        str
            The encoded JWT.

        Raises
        ------
        JwtError
            If there is any error when encoding the token
        """
        now = datetime.datetime.utcnow()

        if expiration_hours != 0:
            payload['exp'] = now + datetime.timedelta(hours=expiration_hours)

        payload['iat'] = now
        payload['timestamp'] = str(datetime.datetime.now())

        try:
            token = jwt.encode(
                payload,
                secret,
                algorithm=algorithm
            )
        except Exception as err:
            raise exceptions.JwtError(err.args)

        return str(token, 'utf-8')

    @staticmethod
    def decode_jwt(token, secret):
        """Decode JWT token using the provided secret key.

        Parameters
        ----------
        token : str
            The JWT token.
        secret : str
            The secret string key used for decoding the token.

        Returns
        -------
        dict
            The JWT payload without reserved claims.

        Raises
        ------
        ExpiredSignatureError
            If token has already expired
        InvalidSignatureError
            If the provided secret key is different from the one that the
            token was signed with
        """
        try:
            payload = jwt.decode(
                token,
                secret,
                algorithms=algorithm
            )
        except jwt.ExpiredSignatureError as err:
            raise exceptions.JwtExpiredSignatureError(err.args)
        except jwt.InvalidSignatureError as err:
            raise exceptions.JwtInvalidSignatureError(err.args)
        except Exception as err:
            raise exceptions.JwtError(err.args)

        clean_payload = {
            key: payload[key] for key in payload if key not in reserved_claims
        }

        return clean_payload
