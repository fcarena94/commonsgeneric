class JwtError(Exception):
    """Base error for any JWT exception."""

    pass


class JwtExpiredSignatureError(JwtError):
    """Raises when the JWT has already expired."""

    pass


class JwtInvalidSignatureError(JwtError):
    """Raised if provided jwt secret key is different from the expected one."""

    pass
