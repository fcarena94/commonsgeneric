from os import environ

from boto3 import client


def get_client():
    """Obtain Ses Client."""
    params = {
        'service_name': 'ses',
        'region_name': environ.get("REGION_NAME"),
        'aws_access_key_id': environ.get("AWS_ACCESS_KEY_ID", ''),
        'aws_secret_access_key': environ.get("AWS_SECRET_ACCESS_KEY", ''),
    }

    if environ.get("ENVIRONMENT") == "devel":
        params["endpoint_url"] = 'http://ses:8567'

    ses_client = client(**params)

    return ses_client


def send_email(subject, body, sender, recipients):
    """Send email using ses client."""
    ses_client = get_client()
    if recipients is not None:
        addresses = recipients.split(",")
        response = ses_client.send_email(
            Source=sender,
            Destination={'ToAddresses': addresses},
            Message={
                'Subject': {
                    'Data': subject,
                    'Charset': 'UTF-8'
                },
                'Body': {
                    'Text': {
                        'Data': body,
                        'Charset': 'UTF-8'
                    }
                }
            }
        )
        return response
