from datetime import datetime
from os import environ

from pytz import timezone


def now():
    """Return current datetime."""
    if environ.get('TIMEZONE') is None:
        raise Exception("The env var 'TIMEZONE' is missing")

    timezone_name = environ.get("TIMEZONE")
    return datetime.now(timezone(timezone_name))
