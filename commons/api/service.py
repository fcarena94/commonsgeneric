from abc import ABC
from datetime import datetime

from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm.exc import (
    MultipleResultsFound as SqlAlchemyMultipleResultsError,
    NoResultFound as SqlAlchemyNoResultError
)

from nwcommons.db import get_session as get_session_func
from .exceptions import (
    MultipleResultsFoundError,
    NoResultFoundError,
    ServiceError,
)


class BaseService(ABC):
    """Base class for services.

    Attributes
    ----------
    _model : Model
        SQLAlchemy model.
    _session : sqlalchemy.session
        sqlalchemy session instance.
    """

    _model = None
    _session = get_session_func

    @classmethod
    def update(cls, attributes):
        """Update a model instance in the DB.

        Parameters
        ----------
        attributes : dict
            the model attributes.

        Returns
        -------
        model
            updated model instance.
        """
        try:
            instance = cls._model(**attributes)
            updated_instance = cls._session().merge(instance)
            cls._session().commit()
        except SQLAlchemyError as err:
            raise ServiceError(err.args)
        else:
            return updated_instance

    @classmethod
    def create(cls, attributes):
        """Create a model instance in the DB.

        Parameters
        ----------
        attributes : dict
            the model attributes.

        Returns
        -------
        model
            created model instance.
        """
        try:
            new_instance = cls._model(**attributes)
            cls._session().add(new_instance)
            cls._session().commit()
        except SQLAlchemyError as err:
            raise ServiceError(err.args)
        else:
            return new_instance

    @classmethod
    def create_all(cls, model_list):
        """Create a list of models in the DB.

        Parameters
        ----------
        model_list : list
            the model list.
        """
        try:
            cls._session().add_all(model_list)
            cls._session().commit()
        except SQLAlchemyError as err:
            raise ServiceError(err.args)

    @classmethod
    def delete(cls, instance):
        """Delete the specified model instance.

        Parameters
        ----------
        instance : Model
            Model instance to delete.
        """
        try:
            instance.delete()
            cls._session().commit()
        except SQLAlchemyError as err:
            raise ServiceError(err.args)

    @classmethod
    def delete_by_params(cls, **kwargs_query):
        """Delete the models that matches the query kwargs.

        Parameters
        ----------
        **kwargs_query : parameters
            parameters for the query with properties as keys.

        Returns
        -------
        int
            amount of deleted registers.
        """
        try:
            deleted_amount = cls.make_query(**kwargs_query).update(
                {"deleted_at": datetime.now()}
            )
            cls._session().commit()
        except SQLAlchemyError as err:
            raise ServiceError(err.args)
        else:
            return deleted_amount

    @classmethod
    def find_one(cls, **kwargs_query):
        """Query to find one instance of the specified model on the DB.

        Parameters
        ----------
        **kwargs_query : parameters
            parameters for the query with properties as keys.

        Returns
        -------
        Model
            model instance that matches with the kwargs or None.

        Raises
        -------
        NoResultFoundError
            when the query selects no rows.
        MultipleResultsFoundError
            when multiple object identities are returned.
        """
        try:
            return cls.make_query(**kwargs_query).one()
        except SqlAlchemyNoResultError as err:
            raise NoResultFoundError(err.args)
        except SqlAlchemyMultipleResultsError as err:
            raise MultipleResultsFoundError(err.args)
        except SQLAlchemyError as err:
            raise ServiceError(err.args)

    @classmethod
    def find_all(cls, **kwargs_query):
        """Query to find all instance of the specified model on the DB.

        Parameters
        ----------
        **kwargs_query : parameters
            parameters for the query with properties as keys.

        Returns
        -------
        list
            list of models instance that matches with the kwargs or None.
        """
        try:
            return cls.make_query(**kwargs_query).all()
        except SQLAlchemyError as err:
            raise ServiceError(err.args)

    @classmethod
    def find_first(cls, **kwargs_query):
        """Query to find first instance of the specified model on the DB.

        Parameters
        ----------
        **kwargs_query : parameters
            parameters for the query with properties as keys.

        Returns
        -------
        Model
            model instance that matches with the kwargs or None.
        """
        try:
            return cls.make_query(**kwargs_query).first()
        except SQLAlchemyError as err:
            raise ServiceError(err.args)

    @classmethod
    def count(cls, **kwargs_query):
        """Query to find the amount of the specified model on the DB.

        Parameters
        ----------
        **kwargs_query : parameters
            parameters for the query with properties as keys.

        Returns
        -------
        int
            amount of database registers that matches with the kwargs.
        """
        try:
            return cls.make_query(**kwargs_query).count()
        except SQLAlchemyError as err:
            raise ServiceError(err.args)

    @classmethod
    def make_query(cls, **kwargs_query):
        """Create query set of specified model.

        Parameters
        ----------
        **kwargs_query : parameters
            parameters for the query with properties as keys.

        Returns
        -------
        queryset
            queryset that matches with the query_dict.
        """
        try:
            return cls._model.query.filter_by(**kwargs_query)
        except SQLAlchemyError as err:
            raise ServiceError(err.args)

    @classmethod
    def get_or_create(cls, attributes):
        """Get a model or creates it if not found by matching attributes.

        Parameters
        ----------
        attributes : dict
            the model attributes.

        Returns
        -------
        model, bool
            new or fetch model instance, True if newly created
        """
        try:
            return cls.find_one(**attributes), False

        except NoResultFoundError:
            instance = cls._model(**attributes)
            cls._session().add(instance)
            cls._session().commit()
            return instance, True
        except SQLAlchemyError as err:
            raise ServiceError(err.args)

    @classmethod
    def save(cls, instance):
        """Add a model instance to session and commits.

        Parameters
        ----------
        instance : model
            the model instance.
        """
        try:
            cls._session().add(instance)
            cls._session().commit()
        except SQLAlchemyError as err:
            raise ServiceError(err.args)
