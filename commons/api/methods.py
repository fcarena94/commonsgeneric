from flask import current_app
from flask.views import MethodView


class BaseMethod(MethodView):
    """Base class for method views."""

    def __init__(self):
        """Initialize the class."""
        MethodView.__init__(self)

        # Validate if service attribute is existent.
        if hasattr(current_app, 'services'):
            self.services = current_app.services
