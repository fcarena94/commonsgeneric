from abc import ABC, abstractmethod
import logging
import os

from flask import current_app

from nwcommons.logger import get_logger


class BaseError(Exception, ABC):
    """Class to subclass by different exception classes."""

    @property
    @abstractmethod
    def code(self):
        """Represent the error with a custom code."""
        pass

    @property
    @abstractmethod
    def description(self):
        """Represent the error description."""
        pass

    def __init__(self, message, context=None, level=logging.ERROR):
        """Initialize class.

        Parameters
        ----------
        message : str
            descriptive error message.
        context : can be converted to string
            info to be logged relative to the error.
        """
        self.message = message
        log_content = {
            "message": message,
            "context": context
        }

        self.logger.log(level, log_content, exc_info=True)

    @property
    def logger(self):
        """Logger: exceptions logger."""
        if current_app:
            logger = current_app.logger
        else:
            logger = get_logger(os.environ.get('API_NAME'))

        return logger


class HTTPError(BaseError):
    """Class to subclass by different exception classes."""

    @property
    @abstractmethod
    def status_code(self):
        """Represent the HTTPStatus value of the error."""
        pass


class ServiceError(Exception):
    """Base error for any exception raised from sqlalchemy."""

    pass


class NoResultFoundError(ServiceError):
    """Raises when no results are found for a query in the DB."""

    pass


class MultipleResultsFoundError(ServiceError):
    """Raises when multiple result are found for a query in the DB."""

    pass
