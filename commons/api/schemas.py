"""Schema improvements for common API actions."""
from decimal import Decimal
import re

from marshmallow import fields, ValidationError
from unidecode import unidecode

from nwcommons.constants import USER_ID_PREFIX


class NotBlankStr(fields.Field):
    """Custom marshmallow field for mandatory values.

    On deserialization validates not blank.
    """

    def _deserialize(self, value, attr, obj, **kwargs):
        if len(value) < 1:
            raise ValidationError('Field cannot be blank.')

        return value


class NoExtendedAsciiStr(fields.Field):
    """
    Custom marshmallow field for values.

    that contain accentuated characters that
    will need to be normalized
    """

    def _deserialize(self, value, attr, obj, **kwargs):

        return unidecode(value)


class NotNullDateTime(fields.DateTime):
    """Extend fields.DateTime.

    Returns empty strings when serializing None values
    """

    def _serialize(self, value, attr, obj, **kwargs):
        """Serialize date to string. In case of None return an empty String."""
        if value is None:
            return ""

        return super()._serialize(value, attr, obj, **kwargs)


class ConvertibleAmountField(fields.Field):
    """Custom marshmallow field for Amount.

    Converts as money amount (float) when serializing.
    Converts as cents (int) when deserializing.
    """

    def _serialize(self, value, attr, obj, **kwargs):
        """Convert value (money amount with decimals) to cents."""
        return int(Decimal(str(value)) * 100) if value is not None else 0

    def _deserialize(self, value, attr, data, **kwargs):
        """Convert value (in cents) to money amount with decimals."""
        return float(value / 100) if value is not None else 0.0


class ConvertibleStringAmountField(fields.Field):
    """Custom marshmallow field for Amount as string.

    Converts as cent money amount (as string) when serializing.
    Converts as money amount (as string) when deserializing.
    """

    def _serialize(self, value, attr, obj, **kwargs):
        """Convert value string (money amount with decimals) to cents."""
        return str(int(Decimal(value) * 100)) if value is not None else "0"

    def _deserialize(self, value, attr, data, **kwargs):
        """Convert value string (in cents) to money amount with decimals."""
        return str(float(value) / 100) if value is not None else "0.0"


class UserIdentifierField(fields.Field):
    """Custom marshmallow field for user identifier.

    Custom field represented as string.
    On serialization removes wallet prefix.
    On deserialization adds wallet prefix.
    """

    def _serialize(self, value, attr, obj, **kwargs):
        if value is None:
            return None
        return value.replace(USER_ID_PREFIX, '')

    def _deserialize(self, value, attr, data, **kwargs):
        if value is None:
            return None
        return "{}{}".format(USER_ID_PREFIX, value)


class SimplifiedConvertibleAmountField(ConvertibleAmountField):
    """Field that serializes to an int when there are no decimal places.

    Converts as money amount (float) when deserializing.
    Return in when amount doesn't have decimal places, float otherwise.
    Converts as cents (int) when serializing.
    """

    def _deserialize(self, value, attr, data, **kwargs):
        """Convert value (in cents) to money amount with decimals.

        Strips decimals if there are none.
        """
        result = super()._deserialize(value, attr, data, **kwargs)
        return result if result % 1 != 0 else int(result)


class DenormalizedPhoneField(fields.Str):
    """Field that serializes a phone without any prefix."""

    def _deserialize(self, value, attr, data, **kwargs):
        """Convert E.164 standard numbering to ugly flexi numbers.

        We strip out most of the prefixes.
        """
        cleanup_matches = ["\\+", "\\-15\\-", "\\-", "^549", "^54", "^0"]
        if value is not None:
            for match in cleanup_matches:
                value = re.sub(match, "", value)
        return value
