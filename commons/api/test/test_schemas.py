import json
import datetime
import marshmallow as ms

from nwcommons.api.schemas import NotBlankStr, NoExtendedAsciiStr, NotNullDateTime, UserFromLMBIdentifierField
from nwcommons.testing import BaseTest


class FakeSchema(ms.Schema):
    """Schema to test custom fields.

    non_blank_field : str
        mandatory field to test not blank validation
    """
    non_blank_field = NotBlankStr(required=True, strict=True)


class FakeNormalizableSchema(ms.Schema):
    """Schema to test custom fields.

    normalizable_str : str
        mandatory field to test field to be normalized
    """
    normalizable_str_field = NoExtendedAsciiStr(required=True, strict=True)


class FakeNotNullDateTimeSchema(ms.Schema):
    """Schema to test custom fields.

    datetime_field : date
        mandatory test field to be converted to string
    """
    datetime_field = NotNullDateTime(format='%Y-%m-%dT%H:%M:%S')


class TestSchemaImprovements(BaseTest):
    def test_not_blank_field_ok(self):
        self.non_blank_data = {
            'non_blank_field': 'string_ok'
        }
        result = FakeSchema().loads(json.dumps(self.non_blank_data))

        assert result == self.non_blank_data

    def test_not_blank_field_on_blank_validation_error(self):
        self.blank_data = {
            'non_blank_field': ''
        }
        expected_error = {'non_blank_field': ['Field cannot be blank.']}
        with self.assertRaises(ms.ValidationError) as error:
            FakeSchema().loads(json.dumps(self.blank_data))

        assert error.exception.messages == expected_error

    def test_normalizable_str_field_ok(self):
        self.normalizable_data = {
            'normalizable_str_field': 'àÀáÁâÂãÃäÄèÈéÉêÊëËìÌíÍîÎïÏòÒóÓôÔõÕöÖùÙúÚûÛüÜýÝÿŸñÑ'
        }

        result = FakeNormalizableSchema().load(self.normalizable_data)

        expected_result = {
            'normalizable_str_field': 'aAaAaAaAaAeEeEeEeEiIiIiIiIoOoOoOoOoOuUuUuUuUyYyYnN'
        }

        assert result == expected_result


class TestNotNullDateTimeSchema(BaseTest):
    def test_datetime_to_string_ok(self):
        self.datetime_to_convert = {
            'datetime_field': datetime.datetime(2020, 3, 25)
        }

        result = FakeNotNullDateTimeSchema().dump(self.datetime_to_convert)

        expected_result = {'datetime_field' : datetime.datetime(2020, 3, 25).strftime('%Y-%m-%dT%H:%M:%S')}

        assert result == expected_result

    def test_none_value_ok(self):
        self.datetime_to_convert = {
            'datetime_field': None
        }

        result = FakeNotNullDateTimeSchema().dump(self.datetime_to_convert)

        expected_result = {'datetime_field': ""}

        assert result == expected_result
