from unittest import TestCase

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import pytest

from nwcommons.api.exceptions import (
    MultipleResultsFoundError,
    NoResultFoundError,
    ServiceError
)
from nwcommons.api.service import BaseService
from nwcommons.db import init_engine
from .models import ExampleModel


class ExampleModelService(BaseService):

    _model = ExampleModel


class TestBaseService(TestCase):

    def setUp(self):
        app = Flask(__name__)
        db = SQLAlchemy(app=app)

        self.session = db.session
        init_engine(self.session)
        ExampleModelService._session = self.session
        ExampleModel.__table__.create(bind=db.get_engine())

    def test_base_service_find_one_method(self):

        with pytest.raises(NoResultFoundError):
            ExampleModelService.find_one(name='Test1')

        new_model1 = ExampleModel(name="Test1")
        new_model2 = ExampleModel(name="Test2")
        self.session.add(new_model1)
        self.session.add(new_model2)
        self.session.commit()

        model = ExampleModelService.find_one(name='Test1')
        assert model == new_model1

        with pytest.raises(MultipleResultsFoundError):
            ExampleModelService.find_one()

    def test_base_service_find_all_method(self):
        model_list = [
            ExampleModel(name="Test1"),
            ExampleModel(name="Test2"),
            ExampleModel(name="Test3"),
        ]
        self.session.add_all(model_list)
        self.session.commit()

        retrieved_list = ExampleModelService.find_all()
        assert type(retrieved_list) == list
        assert len(retrieved_list) == 3

    def test_base_service_find_first_method(self):
        model_list = [
            ExampleModel(name="Test1"),
            ExampleModel(name="Test2"),
            ExampleModel(name="Test3"),
        ]
        self.session.add_all(model_list)
        self.session.commit()

        model = ExampleModelService.find_first()
        assert model == model_list[0]

    def test_base_service_count_method(self):

        amount = ExampleModelService.count()
        assert amount == 0

        model_list = [
            ExampleModel(name="Test1"),
            ExampleModel(name="Test2"),
            ExampleModel(name="Test3"),
        ]
        self.session.add_all(model_list)
        self.session.commit()

        amount = ExampleModelService.count()
        assert amount == 3

    def test_base_service_make_query_method(self):
        model_list = [
            ExampleModel(name="Test1"),
            ExampleModel(name="Test2"),
            ExampleModel(name="Test3"),
        ]
        self.session.add_all(model_list)
        self.session.commit()

        query_set = ExampleModelService.make_query(name='Test1')
        assert query_set.first() == model_list[0]
        assert query_set.count() == 1

        query_set = ExampleModelService.make_query()
        assert query_set.count() == 3
        assert query_set.all() == model_list

    def test_base_service_delete_method(self):
        new_model = ExampleModel(name="Test1")
        self.session.add(new_model)
        self.session.commit()

        model = ExampleModelService.find_one(name='Test1')
        assert model == new_model

        ExampleModelService.delete(model)

        with pytest.raises(NoResultFoundError):
            ExampleModelService.find_one(name='Test1')

    def test_base_service_delete_by_params_method(self):
        new_model = ExampleModel(name="Test1")
        self.session.add(new_model)
        self.session.commit()

        model = ExampleModelService.find_one(name='Test1')
        assert model == new_model

        amount_deleted = ExampleModelService.delete_by_params(name="Test1")
        assert amount_deleted == 1

        amount_deleted = ExampleModelService.delete_by_params(name="Test1")
        assert amount_deleted == 0

        with pytest.raises(NoResultFoundError):
            ExampleModelService.find_one(name='Test1')

    def test_base_service_create_method(self):
        attributes = {
            "name": "NewModel"
        }
        instance = ExampleModelService.create(attributes)

        model = ExampleModelService.find_one(name='NewModel')
        assert model is not None
        assert model.name == "NewModel"
        assert model == instance

    def test_base_service_update_method(self):
        new_model = ExampleModel(name="Test")
        self.session.add(new_model)
        self.session.commit()

        unmodified_model = ExampleModelService.find_first()
        assert unmodified_model.name == "Test"

        attributes = {
            "id": unmodified_model.id,
            "name": "NewName"
        }
        instance = ExampleModelService.update(attributes)
        assert ExampleModelService.count() == 1

        modified_model = ExampleModelService.find_first()
        assert modified_model.name == "NewName"
        assert modified_model.id == unmodified_model.id
        assert instance == modified_model

    def test_base_service_update_method_to_create_an_instance(self):
        assert ExampleModelService.count() == 0

        attributes = {
            "name": "NewModel"
        }
        instance = ExampleModelService.update(attributes)

        assert ExampleModelService.count() == 1
        assert instance.id is not None

    def test_base_service_create_all_method(self):
        assert ExampleModelService.count() == 0

        model_list = [
            ExampleModel(name="Test1"),
            ExampleModel(name="Test2"),
            ExampleModel(name="Test3"),
        ]
        ExampleModelService.create_all(model_list)

        assert ExampleModelService.count() == 3

    def test_base_service_generic_error_return(self):
        with pytest.raises(ServiceError):
            ExampleModelService.find_one(nonExistingAttribute='Test1')

    def test_base_service_get_or_create_method(self):
        assert ExampleModelService.count() == 0

        instance_0, created_0 = ExampleModelService.get_or_create({'name': 'foo'})
        instance_1, created_1 = ExampleModelService.get_or_create({'name': 'foo'})
        instance_2, created_2 = ExampleModelService.get_or_create({'name': 'bar'})

        assert created_0 is True
        assert created_1 is False
        assert created_2 is True
        assert instance_0 == instance_1
        assert instance_0 != instance_2
        assert instance_0.id is not None
        assert instance_1.id is not None
        assert instance_2.id is not None

    def test_gets_or_create_fails_on_multiple_results(self):

        ExampleModelService.create_all(list(
            ExampleModel(name='foo') for _ in range(3)
        ))

        with pytest.raises(ServiceError) as err:
            ExampleModelService.get_or_create({'name': 'foo'})

    def test_base_service_save_method(self):
        assert ExampleModelService.count() == 0

        instance, _ = ExampleModelService.get_or_create({'name': 'foo'})
        instance.name = 'bar'
        ExampleModelService.save(instance)

        ExampleModelService.find_one(name='bar')



