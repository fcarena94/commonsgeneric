from sqlalchemy import Column, String

from nwcommons.db import Base


class ExampleModel(Base):
    """Abstraction with name fields.

    Attributes
    ----------
    name : str
        string name.
    """
    __tablename__ = "test_model"
    name = Column(String, nullable=False)


