import sys

from flask import current_app
from flask.signals import got_request_exception
from flask_restful import Api as Api_
from flask_restful.utils import http_status_message
from werkzeug.datastructures import Headers
from werkzeug.exceptions import HTTPException


class BaseApi(Api_):
    """Basic API class."""

    def add_resource(self, *args, **kwargs):
        """Extend the resource method to be able to filter method types."""
        if "methods" in kwargs:
            resource_class_kwargs = kwargs.get("resource_class_kwargs", {})
            resource_class_kwargs["methods"] = kwargs["methods"]
            kwargs["resource_class_kwargs"] = resource_class_kwargs

        return Api_.add_resource(self, *args, **kwargs)


class Api(BaseApi):
    """Basic API class."""

    def handle_error(self, error):
        """Error handler for the API.

        Transforms a raised exception into a Flask response, with the
        appropriate HTTP status code and body. The current flask_restful
        version is 0.3.7

        Parameters
        ----------
        error : Exception
            the raised Exception object.

        Returns
        -------
        tuple (response, int)
            A response object with the error json data and an HTTP status code.
        """
        got_request_exception.send(
            current_app._get_current_object(), exception=error
        )

        is_http_exception_instance = isinstance(error, HTTPException)

        if not is_http_exception_instance and current_app.propagate_exceptions:
            exc_type, exc_value, tb = sys.exc_info()
            if exc_value is error:
                raise Exception("Error")
            else:
                raise error

        headers = Headers()
        if isinstance(error, HTTPException):
            code = error.code
            default_data = {
                'message': getattr(
                    error, 'description', http_status_message(code))
            }
            headers = error.get_response().headers
        else:
            code = error.status_code
            default_data = {
                'error': error.description,
                'code': error.code,
                'message': error.message
            }

        # Werkzeug exceptions generate a content-length header which is added
        # to the response in addition to the actual content-length header
        # https://github.com/flask-restful/flask-restful/issues/534
        remove_headers = ('Content-Length',)

        for header in remove_headers:
            headers.pop(header, None)

        data = getattr(error, 'data', default_data)

        if code and code >= 500:
            exc_info = sys.exc_info()
            if exc_info[1] is None:
                exc_info = None
            current_app.log_exception(exc_info)

        error_cls_name = type(error).__name__
        if error_cls_name in self.errors:
            custom_data = self.errors.get(error_cls_name, {})
            code = custom_data.get('status', 500)
            data.update(custom_data)

        if code == 406 and self.default_mediatype is None:
            # if we are handling NotAcceptable (406), make sure that
            # make_response uses a representation we support as the
            # default mediatype (so that make_response doesn't throw
            # another NotAcceptable error).
            supported_mediatypes = list(self.representations.keys())
            fallback_mediatype = supported_mediatypes[
                0] if supported_mediatypes else "text/plain"
            resp = self.make_response(
                data,
                code,
                headers,
                fallback_mediatype=fallback_mediatype
            )
        else:
            resp = self.make_response(data, code, headers)

        if code == 401:
            resp = self.unauthorized(resp)
        return resp
