"""Db engine and Base Model initialization module"""
from datetime import datetime
from uuid import uuid4

import pytz

from sqlalchemy import create_engine, Column, DateTime, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.query import Query

engine = None
Base = None


def init_engine(session):
    """Inits the database engine.

    Arguments:
    conn_string -- the database connection string for the engine.
    """

    global engine, Base

    _Base.query = session.query_property(QueryWithSoftDelete)
    _Base.query_all = session.query_property()

    return session


def generate_uuid():
    """Generate a UUID

    Returns
    -------
    str
        uuid string value.
    """
    return str(uuid4())


def create_default_delete_datetime():
    """Return the deleted_at field default date.

    Returns
    -------
    datetime
        default delete date.
    """
    return datetime.fromtimestamp(0, pytz.timezone("UTC"))


def get_session():
    """Return current SQLalchemy session.

    Returns
    -------
    session
        SQLalchemy session.
    """
    from nwcommons.app.flask_app import db
    return db.session


class QueryWithSoftDelete(Query):
    """A Query class to allow the soft-delete behaviour."""

    def __new__(cls, *args, **kwargs):
        """Overriding the Query class __new__."""
        obj = super(QueryWithSoftDelete, cls).__new__(cls)
        if len(args) > 0:
            super(QueryWithSoftDelete, obj).__init__(*args, **kwargs)
            return obj.filter_by(deleted_at=create_default_delete_datetime())
        return obj


class _Base:
    """Base class for declarative_base objects

    Properties
    ----------
    id : str
        default auto generate.
    created_at : datetime
        creation DateTime.
    deleted_at : datetime
        logical deletion DateTime.
    modified_at : datetime
        modify DateTime.

    """

    id = Column(
        String,
        primary_key=True,
        default=generate_uuid
    )
    created_at = Column(
        DateTime(timezone=True),
        default=datetime.now
    )
    deleted_at = Column(
        DateTime(timezone=True),
        default=create_default_delete_datetime
    )
    modified_at = Column(
        DateTime(timezone=True),
        default=None,
        onupdate=datetime.now
    )

    query = None
    query_all = None

    def delete(self):
        """Soft deletes an entity."""
        self.deleted_at = datetime.now()


Base = declarative_base(cls=_Base)
