from os import environ

from flask import current_app
from flask_sqlalchemy import SQLAlchemy

from nwcommons.db import init_engine


def init_sqlalchemy(app):
    """Initialize global and app database session, engine, hook auto
    rollback on both session and add database configuration to app.

    Parameters
    ----------
    app
        Flask app to hook SQLAlchemy.

    """

    app.config['SQLALCHEMY_DATABASE_URI'] = environ.get("DB_CONN_STRING")
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db = SQLAlchemy(app)
    app.db = db
    session = init_engine(app.db.session)
    app.flask_session = app.db.session

    @app.after_request
    def after_request(response):
        session.rollback()
        current_app.flask_session.rollback()
        return response

    @app.teardown_request
    def teardown_request(response):
        """Clear session before the request context is popped from stack.

        Release and close session connections to data base.

        Parameters
        ----------
        response : Flask Response
            current thread response object.

        Returns
        -------
        Flask Response
            same response that was passed as parameter.
        """
        session.rollback()
        current_app.flask_session.rollback()

        session.remove()
        current_app.flask_session.remove()
        return response
