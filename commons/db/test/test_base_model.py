from datetime import datetime
import pytz
from unittest import TestCase

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from .models import BaseNameModelApp
from nwcommons.db import Base, init_engine


class TestBaseModel(TestCase):

    def setUp(self):
        app = Flask(__name__)
        db = SQLAlchemy(app=app)

        self.session = db.session
        init_engine(self.session)
        BaseNameModelApp.__table__.create(bind=db.get_engine())
        self.deleted_at_date = datetime.fromtimestamp(0, pytz.timezone("UTC"))

    def _test_attribute(self, cls, att):
        """Helper function that validates an attribute inside a class."""
        class_ = getattr(self, cls)
        return hasattr(class_, att)

    def test_base_instance(self):
        self.base = Base()

        self.assertTrue(self._test_attribute('base', 'deleted_at'))
        self.assertTrue(self._test_attribute('base', 'created_at'))
        self.assertTrue(self._test_attribute('base', 'modified_at'))
        self.assertTrue(self._test_attribute('base', 'id'))

        # Base doesnt hold __table__ attribute only the classes
        # that inherit from it
        self.assertFalse(self._test_attribute('base', '__table__'))

    def test_base_inheritance(self):
        self.test = BaseNameModelApp(name='testname')
        self.assertTrue(self._test_attribute('test', 'deleted_at'))
        assert self.test.deleted_at is None

        self.test.delete()
        assert self.test.deleted_at is not None

    def test_base_model(self):
        test_register = BaseNameModelApp(name='testname')
        self.session.add(test_register)

        assert test_register.created_at is None
        assert test_register.modified_at is None
        assert test_register.deleted_at is None

        self.session.commit()
        assert test_register.created_at is not None
        assert test_register.modified_at is None
        assert test_register.deleted_at == self.deleted_at_date.replace(tzinfo=None)

        test_register.name = 'testname2'
        self.session.commit()

        assert test_register.name == 'testname2'
        assert test_register.modified_at is not None

        old_modify_date = test_register.modified_at
        test_register.name = 'testname3'
        self.session.commit()

        assert test_register.modified_at != old_modify_date

        test_register.delete()
        self.session.commit()

        assert test_register.deleted_at is not None
