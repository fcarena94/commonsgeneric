from os import environ
from unittest import TestCase

from flask import jsonify
from flask_sqlalchemy import SQLAlchemy

from nwcommons.db import init_engine
from nwcommons.app import bootstrap_app

from .models import BaseNameModelApp, BaseNameModelGlobal


class TestAppDb(TestCase):

    def setUp(self):
        # Set necessary env vars to run bootstrap_app
        environ['FLASK_SECRET_KEY'] = 'KEY'
        environ['DB_CONN_STRING'] = 'sqlite://'
        mock_app = bootstrap_app()
        self.db = SQLAlchemy(app=mock_app)
        init_engine(self.db.session)

        # Create table using both app engine and global engine
        BaseNameModelApp.__table__.create(bind=mock_app.db.engine)
        BaseNameModelGlobal.__table__.create(bind=self.db.get_engine())

        # Create test client and endpoint
        self.client = mock_app.test_client()
        self.headers = {
            "content-type": "application/json",
            "cache-control": "no-cache"
        }

        def base_name_app_db_endpoint():
            # Insert a register into DB using app db instance
            test_register = BaseNameModelApp(name='testname')
            mock_app.flask_session.add(test_register)
            mock_app.flask_session.commit()

            saved_model = mock_app.flask_session.query(BaseNameModelApp).\
                filter_by(
                    id=test_register.id
                ).first()

            assert saved_model == test_register

            response = jsonify({"message": "ok"})
            response.status_code = 200
            return response

        def base_name_global_db_endpoint():
            # Insert a register into DB using global db instance
            test_register = BaseNameModelGlobal(name='testname')
            self.db.session.add(test_register)
            self.db.session.commit()

            saved_model = BaseNameModelGlobal.query.filter_by(
                id=test_register.id
            ).first()

            assert saved_model == test_register

            response = jsonify({"message": "ok"})
            response.status_code = 200
            return response

        mock_app.add_url_rule(
            "/baseNameAppDb",
            "base_name_app_db_endpoint",
            base_name_app_db_endpoint
        )

        mock_app.add_url_rule(
            "/baseNameGlobalDb",
            "base_name_global_db_endpoint",
            base_name_global_db_endpoint
        )

    def test_app_db(self):
        response = self.client.get('/baseNameAppDb', headers=self.headers)
        assert response.status_code == 200
        assert response.get_json() == {"message": "ok"}

    def test_global_db(self):
        response = self.client.get('/baseNameGlobalDb', headers=self.headers)
        assert response.status_code == 200
        assert response.get_json() == {"message": "ok"}
