from sqlalchemy import Column, String

from nwcommons.db import Base


class BaseNameModelApp(Base):
    """Abstraction with name fields.

    Attributes
    ----------
    name : str
        string name.
    """
    __tablename__ = "test_app"
    name = Column(String, nullable=False)


class BaseNameModelGlobal(Base):
    """Abstraction with name fields.

    Attributes
    ----------
    name : str
        string name.
    """
    __tablename__ = "test_global"
    name = Column(String, nullable=False)
