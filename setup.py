import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="commons",
    description="API commons library",
    long_description=long_description,
    packages=setuptools.find_packages(),
    install_requires=[
        'alembic==1.0.7',
        'bcrypt==3.1.6',
        'celery==4.2.1',
        'deepdiff==0.5.2',
        'Flask==1.0.2',
        'Flask-Cors==3.0.7',
        'Flask-Migrate==2.5.2',
        'Flask-RESTful==0.3.7',  # If this version is updated (0.3.7), check if
                                 # handle_error function changes and update it.
        'flask-shell-ipython==0.4.0',
        'Flask-SQLAlchemy==2.4.4',
        'gunicorn==19.9.0',
        'gevent==1.4.0',
        'ipdb==0.11',
        'ipython==5.0.0',
        'marshmallow==3.0.0b18',
        'marshmallow-enum==1.4.1',
        'pika==1.1.0',
        'psycopg2-binary==2.7.7',
        'PyJWT==1.7.1',
        'pytest==5.4.3',
        'pytest-mock==3.1.1',
        'python-json-logger==0.1.10',
        'pytz==2018.9',
        'requests==2.21.0',
        'redis==3.2.1',
        'rollbar==0.14.7',
        'SQLAlchemy==1.3.22',
        'Unidecode==1.1.1',
     ]
)

