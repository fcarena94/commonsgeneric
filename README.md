

## Modules
The modules contained in this package are

### api
Provides the `BaseMethod` and `CustomException` classes



___
### db
Provides both the **sqlalchemy** `session`, `engine` and `Base` instances. 
Also, provides the `generate_uuid()` method.

___
### helpers
Provides the following helpers

##### celery
Provides the `flask_context_celery(app)` method that receives a 
flask application instance and returns a celery instance. To use this 
helper, you must set the following env variables:

* CELERY_BACKEND
* CELERY_BROKER


For more information about celery: http://docs.celeryproject.org/en/latest/index.html`

##### date
Provides the `now()` function that returns the current python datetime in the
specified timezone.

The required env variables that you must set when using this module are:

* TIMEZONE: string indicating the desired timezone.

___
### testing
Contains the `BaseTest`class for testing. Every test class should inherit 
from this one. It contains 2 methods that should be executed in your setUp()
test class

* `run_migrations()` runs database migrations and sets the **session** attr 
to the class. 
* `set_test_client(app)` receives a flask app instance and sets the **client** 
attr of the class with a flask test application that you can use for testing
endpoints

This class inherits from the `TestCase` class from python **unittest** library.

The required env variables that you must set when using this module are:

* ALEMBIC_INI_PATH
* DB_CONN_STRING

___
### logger
Provides a common logger from python **logging** library that you can obtain
by using the `get_logger(name,file_path)` method, being *name* and 
*file_path* optional str. Additionally to stream logs, you can save logs in 
a file if file_path parameter is specified, for example:
`logger = get_logger("testlogger", 'path/to/logfile.log')`
 
 